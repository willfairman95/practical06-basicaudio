/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    audioDeviceManager.initialiseWithDefaultDevices(2, 2); //2 inputs, 2 outputs
    audioDeviceManager.addAudioCallback(this);
    volume.setRange(0.0, 1.0);
    addAndMakeVisible(volume);
    
    audioDeviceManager.setMidiInputEnabled ("USB Axiom 49 Port 1", true);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeAudioCallback(this);
    
}

void MainComponent::resized()
{
    volume.setBounds(getLocalBounds());

}

void MainComponent::handleIncomingMidiMessage(MidiInput* source, const MidiMessage& message)
{
    midiVolume = message.getControllerValue() / 127.0;
    DBG(midiVolume);
    volume.getValueObject().setValue (midiVolume);
}

void MainComponent::audioDeviceIOCallback(const float** inputChannelData, int numInputChannels, float** outputChannelData, int numOutputChannels, int numSamples)
{
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    //DBG("Audio Device IO Callback");
    
    while (numSamples--) {
        *outL = *inL;
        *outR = *inR;
        
        *outL *= midiVolume;
        *outR *= midiVolume;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}

void MainComponent::audioDeviceAboutToStart(AudioIODevice* device)
{
    DBG("Audio Device about to start");
}

void MainComponent::audioDeviceStopped()
{
    DBG("Audio Device Stopped");
}